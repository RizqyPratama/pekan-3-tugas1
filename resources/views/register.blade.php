<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>

<body>

    <form action="/welcome" method="POST">
        @csrf
        <h1>Buat Account Baru!</h1>
        <h3>Sign Up Form</h3>

        <p>First name :</p>
        <input type="text">
        <p>Last name:</p>
        <input type="text">

        <p>Gender:</p>
        <input type="radio" id="male" name="gender"> <label for="male"> Male</label> <br>
        <input type="radio" id="female" name="gender"> <label for="female"> Female</label> <br>
        <input type="radio" id="other" name="gender"> <label for="other"> Other</label>

        <p>Nationality:</p>
        <select name="nationality" id="nationality">
            <option value="indonesia">Indonesia</option>
            <option value="indonesia">Malaysia</option>
            <option value="indonesia">Singapore</option>
            <option value="indonesia">Australia</option>
        </select>

        <p>Languange Spoken:</p>
        <input type="checkbox" name="languange" id="languange1">
        <label for="languange1">Bahasa Indonesia</label> <br>
        <input type="checkbox" name="languange" id="languange2">
        <label for="languange2">English</label> <br>
        <input type="checkbox" name="languange" id="languange3">
        <label for="languange3">Other</label>

        <p>Bio:</p>
        <textarea name="" id="" cols="30" rows="10"></textarea> <br>

        <input type="submit" value="Sign Up">

    </form>


</body>

</html>